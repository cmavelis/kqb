function onSignIn(googleUser) {
    var token = googleUser.getAuthResponse().id_token;
    var existingCookie = Cookies.get().token;
    if(existingCookie && existingCookie == token) {
        return;
    }

    postJSON("/admin/auth", {token: token}).done(function(response) {
        Cookies.set("token", response.token, {path: "/"});
        if(!$("body").hasClass("has-token")) {
            location.reload();
        }
    });
}

function postJSON(url, data) {
    return $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        }
    });
}

function addInputRow(e) {
    var inputs = $("input[name=" + this.name + "]");
    var emptyInputs = inputs.filter(function() { return $(this).val().trim().length == 0; });
    if(emptyInputs.length == 0) {
        var row = $(this).closest(".new-input-row").clone();
        row.find("input").val("").attr("id", "");
        row.insertAfter(inputs[inputs.length-1].closest(".new-input-row"));
        row.find("input.datepicker").removeClass("hasDatepicker").datepicker({dateFormat: "yy-mm-dd"});
    }
}

function getFormData() {
    data = {};
    for(i in arguments) {
        var ipt = $("input[name=" + arguments[i] + "],select[name=" + arguments[i] + "]");
        if(ipt.length) {
            data[arguments[i]] = ipt.val();
        }
    }

    return data;
}

function submitSeason(e) {
    e.stopPropagation();
    e.preventDefault();

    if($(this).closest(".input-form").hasClass("submitting")) {
        return;
    }

    $(this).closest(".input-form").addClass("submitting");

    data = getFormData("id", "name", "start_date", "end_date", "drop_lowest_scores");
    postJSON("/admin/api/season", data).done(function(response) {
        var posts = [];
        $("input[name^=event_]").each(function(i, input) {
            if($(input).val().trim() == "") {
                return;
            }

            var eventData = {event_date: $(input).val(), id: $(input).attr("event_id"), season_id: data.id, points_per_round: 2, points_per_match: 1};
            posts.push(postJSON("/admin/api/event", eventData));
        });

        $.when.apply(this, posts).done(function() {
            location.href = "/admin/season/" + data.id;
        });
    });
}

function submitTeam(e) {
    e.stopPropagation();
    e.preventDefault();

    if($(this).closest(".input-form").hasClass("submitting")) {
        return;
    }

    $(this).closest(".input-form").addClass("submitting");

    data = getFormData("id", "name");
    postJSON("/admin/api/team", data).done(function(response) {
        location.href = "/admin/team/" + response.id;
    });
}

function submitMatch(e) {
    e.stopPropagation();
    e.preventDefault();

    if($(this).closest(".input-form").hasClass("submitting")) {
        return;
    }

    $(this).closest(".input-form").addClass("submitting");

    data = getFormData("id", "event_id", "blue_team_id", "blue_score", "gold_team_id", "gold_score");
    postJSON("/admin/api/match", data).done(function(response) {
        location.href = "/admin/match/" + response.id;
    });
}

function submitPlayer(e) {
    e.stopPropagation();
    e.preventDefault();

    if($(this).closest(".input-form").hasClass("submitting")) {
        return;
    }

    $(this).closest(".input-form").addClass("submitting");

    data = getFormData("id", "name");
    postJSON("/admin/api/player", data).done(function(response) {
        location.href = "/admin/player/" + response.id;
    });
}

function submitEvent(e) {
    e.stopPropagation();
    e.preventDefault();

    if($(this).closest(".input-form").hasClass("submitting")) {
        return;
    }

    $(this).closest(".input-form").addClass("submitting");

    data = getFormData("id", "event_date", "points_per_round", "points_per_match");
    postJSON("/admin/api/event", data).done(function(response) {
        location.href = "/admin/event/" + response.id;
    });
}

function confirmSubmit(e) {
    e.stopPropagation();
    e.preventDefault();

    var row = $(this).closest(".submit-row");
    row.addClass("confirming");

    $(this).hide();
    var confirmButton = $("<input />").addClass("confirm-button").attr("type", "submit").val("Confirm");
    var cancelButton = $("<input />").addClass("cancel-button").attr("type", "submit").val("Cancel");

    row.append(confirmButton, cancelButton);
}

function confirmCancel(e) {
    e.stopPropagation();
    e.preventDefault();

    var row = $(this).closest(".submit-row");
    row.removeClass("confirming");

    row.find(".confirm-button").remove();
    row.find(".cancel-button").remove();
    row.find(".confirm-submit").show();
}

function deleteTeams(e) {
    e.stopPropagation();
    e.preventDefault();

    postJSON("/admin/api/event/delete-teams", {
        event_id: parseInt($("input[name=id]").val())
    }).done(function(data) {
        location.reload();
    });
}

function searchPlayer(e) {
    if($(this).val().trim() == "") {
        clearSearch();
        return;
    }

    var list = $(this).closest(".input-row").find(".player-list");

    $.ajax({
        type: "GET",
        url: "/admin/api/player/search/" + $(this).val().trim().toLowerCase()
    }).done(function(response) {
        list.addClass("active");
        list.empty();

        response.forEach(function(player) {
            if(list.closest(".input-form").find(".readonly-value[player_id=" + player.id + "]").length == 0) {
                $("<a />").addClass("player-selection")
                    .attr("player_id", player.id)
                    .html(player.name)
                    .appendTo(list);
            }
        });

        $("<a />").addClass("player-selection player-selection-new")
            .html("(Add New Player)")
            .appendTo(list);
    });
}

function clearSearch() {
    $(".player-list").removeClass("active");
}

function updatePlayerCount() {
    var playerCount = $(".total-player-count");
    if(playerCount.length > 0) {
        playerCount.html($(".player-list-form .input-row .readonly-value").length);
    }
}

function removePlayer(e) {
    e.stopPropagation();
    e.preventDefault();

    var row = $(this).closest(".input-row");
    var playerID = row.find(".readonly-value").attr("player_id");
    var containerID = $("input[name=id]").val();

    if($(this).closest(".input-form").hasClass("team-players")) {
        url = "/admin/api/team/player/remove";
        data = {"player_id": playerID, "team_id": containerID}
    }

    if($(this).closest(".input-form").hasClass("event-unassigned-players")) {
        url = "/admin/api/event/player/remove";
        data = {"player_id": playerID, "event_id": containerID}
    }

    postJSON(url, data).done(function(response) {
        row.remove();
        updatePlayerCount();
    });
}

function addPlayerToContainer(playerID, containerID, searchRow) {
    var url, data;

    if(searchRow.closest(".input-form").hasClass("team-players")) {
        url = "/admin/api/team/player/add";
        data = {"player_id": playerID, "team_id": containerID}
    }

    if(searchRow.closest(".input-form").hasClass("event-unassigned-players")) {
        url = "/admin/api/event/player/add";
        data = {"player_id": playerID, "event_id": containerID}
    }

    postJSON(url, data).done(function(response) {
        searchRow.find(".player-list").removeClass("active");
        searchRow.find(".player-lookup").focus().val("");

        var row = $("<div />").addClass("input-row");

        $("<a />")
            .addClass("readonly-value")
            .attr("player_id", response.player.id)
            .attr("href", "/admin/player/" + response.player.id)
            .html(response.player.name)
            .appendTo(row);

        $("<a />")
            .addClass("delete-value fa fa-trash")
            .attr("href", "#")
            .appendTo(row);

        row.insertBefore(searchRow);
        updatePlayerCount();
    });
}

function selectPlayer(e) {
    e.stopPropagation();
    e.preventDefault();

    var searchRow = $(this).closest(".input-row");

    if($(this).hasClass("player-selection-new")) {
        postJSON("/admin/api/player", {"name": searchRow.find(".player-lookup").val().trim()}).done(function(data) {
            addPlayerToContainer(data.id, $("input[name=id]").val(), searchRow);
        });
    } else {
        addPlayerToContainer($(this).attr("player_id"), $("input[name=id]").val(), searchRow);
    }
}

function generateTeams(e) {
    e.stopPropagation();
    e.preventDefault();

    postJSON("/admin/api/event/teams", {
        event_id: parseInt($("input[name=id]").val()),
        num_teams: parseInt($("input[name=num_teams]").val())
    }).done(function(data) {
        location.reload();
    });
}

function submitTeamNames(e) {
    var posts = [];
    $(this).closest(".standings").find("input[type=text]").each(function(i, input) {
        if(!$(input).attr("team_id")) {
            return;
        }

        var teamData = {id: $(input).attr("team_id"), name: $(input).val()};
        posts.push(postJSON("/admin/api/team", teamData));
    });

    $.when.apply(this, posts).done(function() {
        location.reload();
    });
}

function matchButtonClick(e) {
    e.stopPropagation();
    e.preventDefault();

    var matchData = {
        "id": parseInt($("input[name=match_id]").val()),
        "blue_score": parseInt($(".match-container .blue-team .score").html()),
        "gold_score": parseInt($(".match-container .gold-team .score").html()),
        "is_complete": false
    };

    var score = 0;
    if($(this).hasClass("plus-1")) {
        score = 1;
    }
    if($(this).hasClass("minus-1")) {
        score = -1;
    }
    if($(this).hasClass("blue")) {
        matchData.blue_score = matchData.blue_score + score;
    }
    if($(this).hasClass("gold")) {
        matchData.gold_score = matchData.gold_score + score;
    }
    if($(this).hasClass("match-complete")) {
        matchData.is_complete = true;
    }

    postJSON("/admin/api/match", matchData).done(function(response) {
        if(response.is_complete) {
            location.reload();
        }

        $(".match-container .blue-team .score").html(response.blue_score);
        $(".match-container .gold-team .score").html(response.gold_score);
    });
}

function matchLinkClick(e) {
    e.stopPropagation();
    e.preventDefault();

    location.href = "/admin/match/" + $(this).attr("match_id");
}

function recalculateRatings(e) {
    e.stopPropagation();
    e.preventDefault();

    if($("body").hasClass("submitting")) {
        return;
    }

    $("body").addClass("submitting");

    postJSON("/admin/api/rating/recalculate", {}).done(function(response) {
        location.reload();
    });
}

function toggleAuthorized(e) {
    e.stopPropagation();
    e.preventDefault();

    if($("body").hasClass("submitting")) {
        return;
    }

    $("body").addClass("submitting");

    data = {
        id: $(this).closest(".row").attr("userid"),
        authorized: $(this).closest(".row").hasClass("authorized") ? false : true
    };

    console.log(data);

    postJSON("/admin/api/user", data).done(function(response) {
        location.reload();
    });
}

$("document").ready(function() {
    $("input.datepicker").datepicker({dateFormat: "yy-mm-dd"});

    $("body")
        .on("change", ".new-input-row input", addInputRow)
        .on("click", ".submit-season", submitSeason)
        .on("click", ".submit-team", submitTeam)
        .on("click", ".submit-match", submitMatch)
        .on("click", ".submit-player", submitPlayer)
        .on("click", ".submit-event", submitEvent)
        .on("click", ".confirm-submit", confirmSubmit)
        .on("click", ".confirming .cancel-button", confirmCancel)
        .on("click", ".delete-teams-row .confirm-button", deleteTeams)
        .on("keyup", ".player-lookup", searchPlayer)
        .on("click", ".player-list-form .delete-value", removePlayer)
        .on("click", ".player-list .player-selection", selectPlayer)
        .on("click", "body", clearSearch)
        .on("click", ".generate-teams", generateTeams)
        .on("click", ".submit-team-names", submitTeamNames)
        .on("click", ".match-container .button", matchButtonClick)
        .on("click", ".event-results .admin-match-link", matchLinkClick)
        .on("click", ".recalculate-all-ratings", recalculateRatings)
        .on("click", ".toggle-authorized", toggleAuthorized)
    ;
});
