#!/bin/bash

basedir="/data/backups"
files="-f docker-compose.yml -f docker-compose.prod.yml"
volumes="-v ${basedir}:${basedir}"
outputdir="${basedir}/$( date +%Y%m%d_%H%M%S )"

cd /data/kqb
/usr/bin/docker-compose $files run $volumes --rm db \
                        pg_dump -h db --user postgres --file "$outputdir" --format directory
