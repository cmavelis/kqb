from django.conf import settings
from django.http import HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.template import loader

from kqb.models import Event, Season, Player


def index(request):
    seasons = list(Season.objects.order_by("-start_date").all())
    season = seasons.pop(0)
    next_event = season.event_set.order_by("event_date").filter(is_complete=False).first()

    if next_event and next_event.current_match():
        template = loader.get_template("event_results.html")
        return HttpResponse(template.render({"event": next_event, "twitch": {"client_id": settings.TWITCH_CLIENT_ID}}, request))
    else:
        template = loader.get_template("player_standings.html")
        return HttpResponse(template.render({"season": season, "other_seasons": seasons}, request))

def player_standings(request, season_id=None):
    if season_id is None:
        season = Season.objects.order_by("-start_date").first()
    else:
        season = Season.objects.get(id=season_id)

    template = loader.get_template("player_standings.html")
    return HttpResponse(template.render({"season": season}, request))

def event_results(request, event_id):
    event = Event.objects.get(id=event_id)

    template = loader.get_template("event_results.html")
    return HttpResponse(template.render({"event": event}, request))

def player_details(request, player_id):
    player = Player.objects.get(id=player_id)

    template = loader.get_template("player_details.html")
    return HttpResponse(template.render({"player": player}, request))
