from django.http import JsonResponse

from kqb.models import Event, Season, Player, Match, Team

def now(request):
    data = {}

    season = Season.objects.order_by("-start_date").first()
    next_event = season.event_set.order_by("event_date").filter(is_complete=False).first()

    if next_event and next_event.current_match():
        current_match = next_event.current_match()
        data["current_match"] = {
            "blue_team": current_match.blue_team.name,
            "blue_score": current_match.blue_score,
            "gold_team": current_match.gold_team.name,
            "gold_score": current_match.gold_score,
        }

        data["standings"] = [
            {
                "team_id": result["team"].id,
                "team_name": result["team"].name,
                "points": "{:0.2f}".format(result["points"]),
                "rounds": result["rounds"],
                "matches": result["matches"],
            }
            for result in next_event.team_results()
        ]

        data["matches"] = [
            {
                "is_complete": match.is_complete,
                "blue_team": match.blue_team.name,
                "blue_score": match.blue_score,
                "gold_team": match.gold_team.name,
                "gold_score": match.gold_score,
            }
            for match in next_event.match_set.order_by("id").all()
        ]

    else:
        if next_event and next_event.event_date:
            data["next_event"] = next_event.event_date.strftime("%Y-%m-%d")

    return JsonResponse(data)
