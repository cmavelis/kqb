import http.client
import json
from django.conf import settings
from django.http import HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from functools import wraps
from google.oauth2 import id_token
from google.auth.transport import requests

from kqb.models import Event, Season, Player, Match, Team, PlayerRating, AdminUser, AdminLoginToken

def auth_required(fn):
    @csrf_exempt
    @wraps(fn)
    def wrapped_func(request, *args, **kwargs):
        if "token" in request.COOKIES:
            user = user_from_local_token(request.COOKIES["token"])
            if user.authorized:
                return fn(request, *args, **kwargs)

        return HttpResponse("Unauthorized", status=http.client.UNAUTHORIZED)

    return wrapped_func

def admin_get(fn):
    @auth_required
    @wraps(fn)
    def wrapped_func(request, *args, **kwargs):
        if request.method != "GET":
            return HttpResponseNotAllowed(["GET"])

        response = fn(request, *args, **kwargs)
        return JsonResponse(response, safe=False)

    return wrapped_func

def admin_post(fn):
    @auth_required
    @wraps(fn)
    def wrapped_func(request, *args, **kwargs):
        if request.method != "POST":
            return HttpResponseNotAllowed(["POST"])

        data = json.loads(request.body)
        response = fn(request, data, *args, **kwargs)
        return JsonResponse(response, safe=False)

    return wrapped_func

def admin_api_post(cls):
    def outer_fn(fn):
        @admin_post
        @wraps(fn)
        def inner_fn(request, data, *args, **kwargs):
            if data.get("id") is None:
                obj = cls()
            else:
                obj = cls.objects.get(id=data["id"])

            for k, v in data.items():
                if k != "id" and not k.startswith("_"):
                    setattr(obj, k, v)

            fn(request, data, obj)
            obj.save()
            return obj_data(obj)

        return inner_fn
    return outer_fn

def obj_data(obj):
    return {k: v for k, v in obj.__dict__.items() if not k.startswith("_")}

def user_from_google_token(token):
    try:
        idinfo = id_token.verify_oauth2_token(token, requests.Request(), settings.OAUTH_CLIENT_ID)
        if idinfo["iss"] not in ["accounts.google.com", "https://accounts.google.com"]:
            return None

        user = AdminUser.by_account_id(idinfo["sub"])
        if user.id is None:
            user.email = idinfo["email"]
            user.save()

        return user

    except ValueError:
        return None

def user_from_local_token(token):
    try:
        token = AdminLoginToken.objects.filter(token=token).get()
        return token.admin_user

    except:
        return None

def render_template(request, template_name, context=None):
    if context is None:
        context = {}

    context["settings"] = settings
    if "token" in request.COOKIES:
        context["adminuser"] = user_from_local_token(request.COOKIES["token"])

    template = loader.get_template(template_name)
    return HttpResponse(template.render(context, request))

def indexpage(request):
    seasons = Season.objects.order_by("start_date").all()
    return render_template(request, "admin_index.html", {"seasons": seasons})

@csrf_exempt
def auth(request):
    data = json.loads(request.body)
    user = user_from_google_token(data["token"])
    if user.authorized:
        return JsonResponse({"token": user.generate_token().token})

    return HttpResponse("Unauthorized", status=http.client.UNAUTHORIZED)

def season(request, season_id=None):
    context = {}
    if season_id:
        context["season"] = Season.objects.get(id=season_id)

    return render_template(request, "admin_season.html", context)

def event(request, event_id):
    event = Event.objects.get(id=event_id)
    return render_template(request, "admin_event.html", {"event": event})

def team(request, team_id):
    team = Team.objects.get(id=team_id)
    return render_template(request, "admin_team.html", {"team": team})

def match(request, match_id=None, event_id=None):
    context = {}
    if match_id is not None:
        context["match"] = Match.objects.get(id=match_id)
        context["event"] = context["match"].event
    if event_id is not None:
        context["event"] = Event.objects.get(id=event_id)

    return render_template(request, "admin_match.html", context)

def players(request):
    players = Player.objects.order_by("name").all()
    season = Season.objects.order_by("-start_date").first()
    results = {i["player"].id: i for i in season.player_results()}
    return render_template(request, "admin_players.html", {"players": players, "season": season, "results": results})

def player_detail(request, player_id):
    player = Player.objects.get(id=player_id)
    ratings = player.playerrating_set.order_by("rating_date").all()
    ratings_zipped = zip(ratings, [None] + list(ratings))
    return render_template(request, "admin_player_detail.html", {"player": player, "ratings": ratings_zipped})

def users(request):
    users = AdminUser.objects.order_by("email").all()
    return render_template(request, "admin_users.html", {"users": users})

@admin_api_post(Season)
def post_season(request, data, season):
    pass

@admin_api_post(Event)
def post_event(request, data, event):
    pass

@admin_api_post(Player)
def post_player(request, data, event):
    pass

@admin_api_post(Match)
def post_match(request, data, match):
    if match.is_complete:
        if match.event.match_set.filter(is_complete=False).exclude(id=match.id).count() == 0:
            match.event.is_complete = True
            match.event.save()

        if match.playerrating_set.count() == 0:
            match.rate_players()

@admin_api_post(Team)
def post_team(request, data, team):
    pass

@admin_api_post(AdminUser)
def post_user(request, data, team):
    pass

@admin_post
def team_add_player(request, data):
    team = Team.objects.get(id=data["team_id"])
    player = Player.objects.get(id=data["player_id"])

    team.players.add(player)
    return {"team": obj_data(team), "player": obj_data(player)}

@admin_post
def team_remove_player(request, data):
    team = Team.objects.get(id=data["team_id"])
    player = Player.objects.get(id=data["player_id"])

    team.players.remove(player)
    return {"team": obj_data(team), "player": obj_data(player)}

@admin_post
def event_add_player(request, data):
    event = Event.objects.get(id=data["event_id"])
    player = Player.objects.get(id=data["player_id"])

    event.unassigned_players.add(player)
    return {"event": obj_data(event), "player": obj_data(player)}

@admin_post
def event_remove_player(request, data):
    event = Event.objects.get(id=data["event_id"])
    player = Player.objects.get(id=data["player_id"])

    event.unassigned_players.remove(player)
    return {"event": obj_data(event), "player": obj_data(player)}

@admin_post
def event_generate_teams(request, data):
    event = Event.objects.get(id=data["event_id"])
    event.generate_teams(data["num_teams"])

    return {"event": obj_data(event), "teams": [obj_data(i) for i in event.team_set.all()]}

@admin_post
def event_delete_teams(request, data):
    event = Event.objects.get(id=data["event_id"])

    event.match_set.all().delete()

    for team in event.team_set.all():
        event.unassigned_players.add(*team.players.all())
        team.delete()

@admin_get
def search_player(request, searchval):
    return [obj_data(i) for i in Player.objects.filter(name__icontains=searchval).order_by("name")]

@admin_post
def rating_recalculate(request, data):
    PlayerRating.recalculate_all()
