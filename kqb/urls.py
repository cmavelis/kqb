from django.urls import path

from . import views
from .views import admin, api

urlpatterns = [
    path("", views.index, name="index"),
    path("season/<int:season_id>", views.player_standings, name="season_standings"),
    path("event/<int:event_id>", views.event_results, name="event_results"),
    path("player/<int:player_id>", views.player_details, name="player_details"),

    path("api/now", api.now, name="api_now"),

    path("admin/", admin.indexpage, name="admin_index"),
    path("admin/auth", admin.auth, name="admin_auth"),
    path("admin/season/<int:season_id>", admin.season, name="admin_season"),
    path("admin/season/new", admin.season, name="admin_season"),
    path("admin/event/<int:event_id>", admin.event, name="admin_event"),
    path("admin/event/<int:event_id>/match/new", admin.match, name="admin_match_new"),
    path("admin/team/<int:team_id>", admin.team, name="admin_team"),
    path("admin/match/<int:match_id>", admin.match, name="admin_match"),
    path("admin/players", admin.players, name="admin_players"),
    path("admin/player/<int:player_id>", admin.player_detail, name="admin_player_detail"),
    path("admin/users", admin.users, name="admin_users"),

    path("admin/api/season", admin.post_season, name="admin_post_season"),
    path("admin/api/event", admin.post_event, name="admin_post_event"),
    path("admin/api/player", admin.post_player, name="admin_post_player"),
    path("admin/api/match", admin.post_match, name="admin_post_match"),
    path("admin/api/team", admin.post_team, name="admin_post_team"),
    path("admin/api/user", admin.post_user, name="admin_post_user"),
    path("admin/api/team/player/add", admin.team_add_player, name="admin_team_add_player"),
    path("admin/api/team/player/remove", admin.team_remove_player, name="admin_team_remove_player"),
    path("admin/api/event/teams", admin.event_generate_teams, name="admin_event_generate_teams"),
    path("admin/api/event/delete-teams", admin.event_delete_teams, name="admin_event_delete_teams"),
    path("admin/api/event/player/add", admin.event_add_player, name="admin_event_add_player"),
    path("admin/api/event/player/remove", admin.event_remove_player, name="admin_event_remove_player"),
    path("admin/api/player/search/<searchval>", admin.search_player, name="admin_search_player"),
    path("admin/api/rating/recalculate", admin.rating_recalculate, name="admin_rating_recalculate"),
]
