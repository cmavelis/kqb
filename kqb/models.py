import hashlib
import itertools
import os
import random
import trueskill
from datetime import datetime
from django.db import models, transaction


trueskill_env = trueskill.TrueSkill(draw_probability=0.0)


class Player(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def results_by_season(self):
        seasons = {}

        for team in self.team_set.all():
            event = team.event
            season = event.season
            if season.id not in seasons:
                seasons[season.id] = {"season": season, "events": {}}

            event_result = seasons[season.id]["events"][event.id] = team.results()

        season_results_sorted = []
        for season in sorted(seasons.values(), key=lambda x: x["season"].start_date):
            season_result = {"season": season["season"], "events": [], "rounds": 0, "matches": 0, "points": 0}

            season_result["events"] = sorted(season["events"].values(), key=lambda x: x["team"].event.event_date)

            max_events_counted = season["season"].event_set.count() - season["season"].drop_lowest_scores
            for event_result in sorted(season_result["events"], key=lambda x: x["points"], reverse=True)[:max_events_counted]:
                for k in ["rounds", "matches", "points"]:
                    season_result[k] += event_result[k]

            season_results_sorted.append(season_result)

        return season_results_sorted

    @classmethod
    def by_name(cls, name):
        try:
            return cls.objects.filter(name=name).get()
        except cls.DoesNotExist:
            return cls(name=name)

    def rating(self):
        rating = PlayerRating.objects.filter(player=self).order_by("-rating_date").first()
        if rating is None:
            return trueskill_env.create_rating()
        else:
            return rating.rating()


class Season(models.Model):
    name = models.CharField(max_length=200, unique=True)
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)
    drop_lowest_scores = models.IntegerField(default=0)
    match_count_base = models.IntegerField(default=0)

    def player_results(self):
        results_by_player = {}

        for event in self.event_set.all():
            team_results = event.team_results()

            for team_result in team_results:
                for player in team_result["team"].players.all():
                    if player.id not in results_by_player:
                        results_by_player[player.id] = {"player": player, "events": []}

                    results_by_player[player.id]["events"].append(team_result)

        results = []
        max_events_counted = self.event_set.count() - self.drop_lowest_scores
        for player_id, player_results in results_by_player.items():
            result = {"points": 0, "rounds": 0, "matches": 0, "player": player_results["player"]}
            player_results["events"].sort(key=lambda x: x["points"], reverse=True)
            for event_result in player_results["events"][:max_events_counted]:
                for k in ["rounds", "matches", "points"]:
                    result[k] += event_result[k]

            results.append(result)

        return sorted(results, key=lambda x: x["points"], reverse=True)


class Event(models.Model):
    event_date = models.DateTimeField()
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    is_complete = models.BooleanField(default=False)
    unassigned_players = models.ManyToManyField(Player)
    points_per_round = models.IntegerField(default=0)
    points_per_match = models.IntegerField(default=0)

    def team_results(self):
        results = {}

        for team in self.team_set.all():
            results[team.id] = team.results()

        return sorted(results.values(), key=lambda x: (x["points"], -x["team"].id), reverse=True)

    def event_number(self):
        return self.season.event_set.filter(event_date__lte=self.event_date).count()

    def generate_teams(self, num_teams):
        teams = [Team(event=self, name="Team #{}".format(i+1)) for i in range(num_teams)]
        Team.objects.bulk_create(teams)

        if self.unassigned_players.count() > 0:
            players = self.unassigned_players.all()

            player_points = {i["player"].id: i["points"] for i in self.season.player_results()}
            player_ratings = {i.id: i.rating().mu for i in players}

            season_pct_complete = self.event_number() / self.season.event_set.count()

            if player_points:
                points_weight = season_pct_complete * (max(player_points.values()) - min(player_points.values()))
                rating_weight = (1 - season_pct_complete) * (max(player_ratings.values()) - min(player_ratings.values()))
            else:
                points_weight = 0
                rating_weight = 1

            rating_sort = lambda i: (player_points.get(i.id, 0) * points_weight + player_ratings.get(i.id, 0) * rating_weight)

            ranked_players = sorted(players, key=rating_sort, reverse=True)

            for player, team in zip(ranked_players, itertools.cycle(teams + teams[::-1])):
                team.players.add(player)

            self.unassigned_players.clear()

        self.generate_matches()

    def generate_matches(self):
        schedules_by_team_count = {
            3: [
                (0, 1),
                (0, 2),
                (1, 2),
                (1, 0),
                (2, 0),
                (2, 1),
            ],
            4: [
                (0, 1),
                (2, 3),
                (0, 2),
                (1, 3),
                (1, 2),
                (0, 3),
            ],
            5: [
                (0, 1),
                (2, 3),
                (0, 4),
                (1, 2),
                (3, 4),
                (0, 2),
                (1, 3),
                (2, 4),
                (0, 3),
                (1, 4),
            ],
            6: [
                (0, 1),
                (2, 3),
                (4, 5),
                (0, 2),
                (1, 3),
                (2, 4),
                (3, 5),
                (1, 4),
                (0, 3),
                (2, 5),
                (0, 4),
                (1, 5),
                (3, 4),
                (0, 5),
                (1, 2),
            ],
        }

        teams = self.team_set.order_by("id").all()

        if len(teams) in schedules_by_team_count:
            schedule = schedules_by_team_count[len(teams)]
        else:
            schedule = [sorted(i, key=lambda x: random.random()) for i in itertools.combinations(range(len(teams)))]
            random.shuffle(schedule)

        for matchup in schedule:
            Match(event=self, blue_team=teams[matchup[0]], gold_team=teams[matchup[1]]).save()

    def current_match(self):
        return self.match_set.filter(is_complete=False).order_by("id").first()


class Team(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    players = models.ManyToManyField(Player)

    def results(self):
        results = {"team": self, "rounds": 0, "matches": 0}
        for match in self.matches():
            scores = match.score_by_team_id()
            results["rounds"] += scores[self.id]
            if match.is_complete and scores[self.id] == max(scores.values()):
                results["matches"] += 1

        results["points"] = self.event.points_per_round * results["rounds"] + \
                            self.event.points_per_match * results["matches"]

        match_count_base = self.event.season.match_count_base
        if match_count_base and len(self.matches()) > 0:
            results["points"] *= (match_count_base) / (len(self.matches()))

        return results

    def matches(self):
        return Match.objects.filter(blue_team_id=self.id) | Match.objects.filter(gold_team_id=self.id)


class Match(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    blue_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="+")
    gold_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="+")
    blue_score = models.IntegerField(default=0)
    gold_score = models.IntegerField(default=0)
    is_complete = models.BooleanField(default=False)

    def score_by_team_id(self):
        return {
            self.blue_team_id: self.blue_score,
            self.gold_team_id: self.gold_score,
        }

    def rate_players(self):
        blue_players = self.blue_team.players.all()
        gold_players = self.gold_team.players.all()
        blue_ratings = {player: player.rating() for player in blue_players}
        gold_ratings = {player: player.rating() for player in gold_players}

        ranks = [0 if self.blue_score > self.gold_score else 1, 0 if self.gold_score > self.blue_score else 1]
        rating_groups = trueskill_env.rate([blue_ratings, gold_ratings], ranks=ranks)
        for group in rating_groups:
            for player, rating in group.items():
                PlayerRating(player=player, match=self, mu=rating.mu, sigma=rating.sigma).save()


class PlayerRating(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    rating_date = models.DateTimeField(default=datetime.now)
    mu = models.FloatField(default=0)
    sigma = models.FloatField(default=0)

    def rating(self):
        return trueskill_env.create_rating(mu=self.mu, sigma=self.sigma)

    @classmethod
    @transaction.atomic
    def recalculate_all(cls):
        cls.objects.all().delete()

        for event in Event.objects.order_by("event_date").all():
            for match in event.match_set.filter(is_complete=True).order_by("id").all():
                match.rate_players()


class AdminUser(models.Model):
    account_id = models.CharField(max_length=200, unique=True)
    email = models.CharField(max_length=200)
    authorized = models.BooleanField(default=False)

    @classmethod
    def by_account_id(cls, account_id):
        try:
            return cls.objects.filter(account_id=account_id).get()
        except cls.DoesNotExist:
            return cls(account_id=account_id)

    def generate_token(self):
        token = AdminLoginToken(admin_user=self, token=hashlib.sha512(os.urandom(2048)).hexdigest())
        token.save()

        return token


class AdminLoginToken(models.Model):
    admin_user = models.ForeignKey(AdminUser, on_delete=models.CASCADE)
    issue_date = models.DateTimeField(default=datetime.now)
    token = models.CharField(max_length=128, unique=True)
