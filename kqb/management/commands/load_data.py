import pytz
from datetime import datetime
from django.core.management.base import BaseCommand
from kqb.models import Player, Season, Event, Team, Match

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        season = Season(
            name="Summer 2018 Season",
            start_date=datetime(year=2018, month=6, day=13, tzinfo=pytz.timezone("US/Eastern")),
            end_date=datetime(year=2018, month=8, day=1, tzinfo=pytz.timezone("US/Eastern")),
        )
        season.save()

        event = Event(season=season, event_date=datetime(year=2018, month=6, day=13, tzinfo=pytz.timezone("US/Eastern")), is_complete=True)
        event.save()

        t1 = Team(event=event, name="Bee Positive")
        t1.save()

        players = [Player.by_name(i) for i in ["Todd", "Sebastian", "Joe Sklover", "Robin W"]]
        Player.objects.bulk_create(filter(lambda x: x.id is None, players))
        t1.players.add(*players)

        t2 = Team(event=event, name="Team Name")
        t2.save()

        players = [Player.by_name(i) for i in ["Clarkson", "Laura", "Mike T", "Waffles", "Karina"]]
        Player.objects.bulk_create(filter(lambda x: x.id is None, players))
        t2.players.add(*players)

        t3 = Team(event=event, name="Bee Punny")
        t3.save()

        players = [Player.by_name(i) for i in ["Vincent", "Matt Schwartz", "Corey Cossentino", "Chris Presinger", "Danny"]]
        Player.objects.bulk_create(filter(lambda x: x.id is None, players))
        t3.players.add(*players)

        t4 = Team(event=event, name="Berry Irresistable")
        t4.save()

        players = [Player.by_name(i) for i in ["Steve Parker", "Brendan", "Rachel", "Dom"]]
        Player.objects.bulk_create(filter(lambda x: x.id is None, players))
        t4.players.add(*players)

        t5 = Team(event=event, name="Brangasus")
        t5.save()

        players = [Player.by_name(i) for i in ["Ronny Fernandez", "Max", "Shrek", "James", "Steph"]]
        Player.objects.bulk_create(filter(lambda x: x.id is None, players))
        t5.players.add(*players)

        Match.objects.bulk_create([
            Match(event=event, blue_team=t1, gold_team=t2, blue_score=2, gold_score=3, is_complete=True),
            Match(event=event, blue_team=t3, gold_team=t4, blue_score=3, gold_score=2, is_complete=True),
            Match(event=event, blue_team=t1, gold_team=t5, blue_score=3, gold_score=0, is_complete=True),
            Match(event=event, blue_team=t2, gold_team=t3, blue_score=0, gold_score=3, is_complete=True),
            Match(event=event, blue_team=t4, gold_team=t5, blue_score=3, gold_score=2, is_complete=True),
            Match(event=event, blue_team=t1, gold_team=t3, blue_score=2, gold_score=3, is_complete=True),
            Match(event=event, blue_team=t2, gold_team=t4, blue_score=1, gold_score=3, is_complete=True),
            Match(event=event, blue_team=t3, gold_team=t5, blue_score=3, gold_score=1, is_complete=True),
            Match(event=event, blue_team=t1, gold_team=t4, blue_score=0, gold_score=3, is_complete=True),
            Match(event=event, blue_team=t2, gold_team=t5, blue_score=3, gold_score=1, is_complete=True),
        ])


        event = Event(season=season, event_date=datetime(year=2018, month=6, day=20, tzinfo=pytz.timezone("US/Eastern")), is_complete=True)
        event.save()

        t1 = Team(event=event, name="BuzzFeed LLC")
        t1.save()

        players = [Player.by_name(i) for i in ["Clarkson", "Sebastian", "Matt Schwartz", "Joe Sklover"]]
        Player.objects.bulk_create(filter(lambda x: x.id is None, players))
        t1.players.add(*players)

        t2 = Team(event=event, name="Honey Mustard")
        t2.save()

        players = [Player.by_name(i) for i in ["Todd", "Kevin", "Shrek", "Morgan", "Mike Pinkin"]]
        Player.objects.bulk_create(filter(lambda x: x.id is None, players))
        t2.players.add(*players)

        t3 = Team(event=event, name="Team Name")
        t3.save()

        players = [Player.by_name(i) for i in ["Chris Presinger", "Brendan", "Rachel", "Karina"]]
        Player.objects.bulk_create(filter(lambda x: x.id is None, players))
        t3.players.add(*players)

        t4 = Team(event=event, name="Stinger Bell")
        t4.save()

        players = [Player.by_name(i) for i in ["Steve Parker", "Dom", "Mike T", "Robin W", "Melanie"]]
        Player.objects.bulk_create(filter(lambda x: x.id is None, players))
        t4.players.add(*players)

        Match.objects.bulk_create([
            Match(event=event, blue_team=t1, gold_team=t2, blue_score=3, gold_score=2, is_complete=True),
            Match(event=event, blue_team=t3, gold_team=t4, blue_score=1, gold_score=3, is_complete=True),
            Match(event=event, blue_team=t1, gold_team=t3, blue_score=3, gold_score=0, is_complete=True),
            Match(event=event, blue_team=t2, gold_team=t4, blue_score=0, gold_score=3, is_complete=True),
            Match(event=event, blue_team=t2, gold_team=t3, blue_score=1, gold_score=3, is_complete=True),
            Match(event=event, blue_team=t1, gold_team=t4, blue_score=3, gold_score=0, is_complete=True),
        ])
